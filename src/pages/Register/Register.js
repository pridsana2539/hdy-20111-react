import React from "react";
import { register } from "../../api/api";
import RegisterForm from '../../components/RegisterForm'
export default function Register(props) {
  const save = async (user) => {
    let result = await register(user)
    props.history.push('/login')
    // console.log(save);
    
  }
  return (
    <div class="container  btn-light btn-lg">
      <h1 style={{ textAlign: "center" }}> REGISTER </h1>
      <div style={{ textAlign: "center" }}>
      <img src={ process.env.PUBLIC_URL + 'assets/images/regis.png' } width="180"></img>
      </div>
    <RegisterForm save={save} />
    </div>
  );
}