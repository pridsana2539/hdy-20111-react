import React, { useEffect, useState } from "react";
import ProductForm from "../../components/ProductForm";
import  { getAllProduct, editProduct } from '../../api/api';

export default function Edit(props) {
    const [product, setProduct] = useState();

    useEffect(() => {
      const fetchApi = async () => {
        let result = await getAllProduct(props.match.params.id)
        
        
        let data = result.data.filter((items) => {
         
            return items._id === props.match.params.id;
            console.log(items._id);
          });
          
        setProduct(data[0]);
      }
      fetchApi();
    }, []);
  

  const edit = async (product) => {
    let edit = await editProduct(props.match.params.id, product)
    if (edit.status === "success") {
      props.history.push('/showproduct')
    } else{
        alert(edit.message)
    }
 
  }

  return (
    <div>

   {/* <FormProduct    product={product} edit={edit} />  */}
      {product !== undefined && <ProductForm check="Edit" product={product} edit={edit} />}

    </div>
  );
}