import React, { useEffect, useState } from "react";
import ProductForm from "../../components/ProductForm";
import MyProductForm from "../../components/MyProductForm";
import { getAllProduct,getIdProduct, editProduct } from "../../api/api";

export default function EditProduct(props) {
    const [product, setProduct] = useState();

    useEffect(() => {
      const fetchApi = async () => {
        let result = await getAllProduct(props.match.params.id)
        let data = result.data.filter((items) => {
            return items._id === props.match.params.id;
          });
        console.log(data[0])
        setProduct(data[0])
      }
      fetchApi();
    }, [props.match.params.id]);
  

  const edit = async (product) => {
    let edit = await editProduct(props.match.params.id, product)
    if (edit.status === "success") {
      props.history.push('/home')
    } else{
        alert(edit.message)
    }
  }
  
  return (
    <div>
      {/* <Back url="/home" history={props.history} /> */}
      <h1>Edit Product</h1>
      <hr />
      {product !== undefined && <ProductForm check="EditProduct" product={product} edit={edit} />}
    </div>
  );
}