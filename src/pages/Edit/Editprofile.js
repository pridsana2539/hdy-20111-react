import React, { useEffect, useState } from "react";
import RegisterForm from '../../components/RegisterForm'
import { getAllByIdUser, editUser } from "../../api/api";

export default function Editprofile(props) {
    const [user, setUser] = useState();

    useEffect(() => {
      const fetchApi = async () => {
        let result = await getAllByIdUser(props.match.params.id);
        setUser(result.data);
      };
      fetchApi();
    }, []);

    const edit = async (user) => {
       // console.log(user);
        let edit = await editUser(props.match.params.id, user)
        if (edit.status === "success") {
          props.history.push('/home')
        } else{
            alert(edit.message)
    
        }
      }
      
    return (
        <div>
         
            <h1>Edit Profile</h1>
           <RegisterForm  check="Edit" user={user} edit={edit}/>
        </div>
    )
}
