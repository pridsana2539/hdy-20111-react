import React, { useState, useEffect } from 'react'
import ProductTable from '../../components/ProductTable'
import ProductTitle from '../../components/ProductTitle'
import ProductCreate from '../../components/ProductCreate'
import EditProduct from '../../pages/Edit/EditProduct'
import { getAllProduct, deleteUser } from '../../api/api'

export default function Product(props) {
  const [user, setUser] = useState([])

  const fetchUser = async () => {
    let result = await getAllProduct()
    console.log(result)
    setUser(result.data)
  }

  useEffect(() => {
      fetchUser()
  }, [])

  const nextCreateProduct = () => {
    props.history.push('/createProduct')
  }

  const removeUser = async (id) => {
   let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
      let result = await deleteUser(id)
      console.log(result)
      if (result.status === "success") {
        fetchUser()
      }
    }
  }

  return (
    <div>
      <ProductCreate nextCreateProduct={nextCreateProduct}/>
      <ProductTitle/>
      <ProductTable user={user} delete={removeUser}/>
    </div>
  )
}
