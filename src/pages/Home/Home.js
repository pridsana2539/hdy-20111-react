import React, { useState, useEffect } from 'react'
import ProductTable from '../../components/ProductTable'
//import ProductTitle from '../../components/ProductTitle'
import MyProductForm from '../../components/MyProductForm'
import HomeCreateButton from '../../components/HomeCreateButton'
import { getAllProduct, deleteProduct } from '../../api/api'

export default function Home(props) {
  const [product, setProduct] = useState([])

  const fetchProduct = async () => {
    let result = await getAllProduct()
    console.log(result)
    setProduct(result)
}

  useEffect(() => {
      fetchProduct()
  }, [])

   const nextCreate = () => {
   props.history.push('/createproduct')
  }

  const removeProduct = async (id) => {
    let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
    let result = await deleteProduct(id)
    if(result.status === "success"){
    //console.log(result)
    fetchProduct()
    }
  }
}

  return (
    <div class="container btn-secondary btn-lg">
      <h1> Products Stocks </h1>
     <HomeCreateButton nextCreate={nextCreate}/>
       <hr/>
  <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">My Products</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">All Products</a>
    {/* <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a> */}
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"><MyProductForm product={ product } delete={removeProduct}/></div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab"><ProductTable product={ product } delete={removeProduct}/></div>
 
</div>
</div>
  )
}