import React, { useState, useEffect } from 'react'
// import ProductTable from '../../components/ProductTable'
import MyProductTitle from '../../components/MyProductTitle'
import MyProductTable from '../../components/MyProductTable'
import { getAllProduct, deleteUser } from '../../api/api'

export default function Product(props) {
  const [user, setUser] = useState([])

  const fetchUser = async () => {
    let result = await getAllProduct()
    console.log(result)
    setUser(result.data)
  }

  useEffect(() => {
      fetchUser()
  }, [])

  const nextmyProduct = () => {
    props.history.push('/myProduct')
  }

  const removeUser = async (id) => {
   let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
      let result = await deleteUser(id)
      console.log(result)
      if (result.status === "success") {
        fetchUser()
      }
    }
  }

  return (
    <div>
      <nextMyProduct nextMyProduct={nextMyProduct}/>
      <MyProductTitle/>
      <MyProductTable user={user} delete={removeUser}/>
    </div>
  )
}
