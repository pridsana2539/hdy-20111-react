import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getAllByIdUser } from "../../api/api";
export default function Profile(props) {
  const [userProfile, setUserProfile] = useState([]);

  var id = localStorage.getItem('Id');
  useEffect(() => {
  fetchUser();
  }, []);

  const fetchUser = async () => {
    await getAllByIdUser(id).then((res) => {
      if (res.status === "success") {
        setUserProfile(res.data);
      
      }
    });
  };
    return (

  <div class="card text-white bg-info mb-9" >
  <div style={{ textAlign: "center" }}>
   <h1>My Profile</h1>
  
    <div class="col">
      <img src="assets/images/pro.png" width="150" height="150" className="d-inline-block align-top" alt=""></img> 
    </div>
    </div>
    <div class="card-body">
    <label><h4>UserName : {userProfile.username}</h4></label><br></br>
    <label><h4>Password :  {userProfile.password}</h4></label><br></br>
    <label><h4>Name :  {userProfile.name}</h4></label><br></br>
    <label><h4>Age: {userProfile.age} </h4></label><br></br>
    <label><h4>Selary : {userProfile.salary}</h4></label> 

      <Link button type="submit" className="btn btn-dark float-right" to={`/editprofile/${userProfile._id}`}  >
     Edit Profile
      </Link>  
  </div></div>

    )
}