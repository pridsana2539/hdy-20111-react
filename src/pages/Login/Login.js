import React, { useState } from "react";
// import { login } from "../../api/api";
import LoginForm from '../../components/LoginForm'
import { loginTest } from "../../api/api";
// import "./Login.css";
export default function Login(props) {
    // const [user, setUser] = useState();

    const login = async (user,id) => {
        let result = await loginTest(user)
        if(result.status === "success"){
          localStorage.setItem('Id', result.data._id);
          localStorage.setItem('Usename', result.data.usename);
         props.history.push('/home')
         console.log(result.data._id);
         
        }

        else{
            alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')
            console.log(result.status );
            }
        }
            
    return (
        <div  class="container btn-light btn-lg ">
            <h1 style={{ textAlign: "center", color: "//#region " }}>LOGIN</h1>
            <div style={{ textAlign: "center" }}>
            <img src={process.env.PUBLIC_URL + 'assets/images/a.png'} width="180"></img>

            </div>
            <LoginForm check="Login" login={login} />
        </div>
    );
}



