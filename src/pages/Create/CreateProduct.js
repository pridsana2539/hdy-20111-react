import React from 'react'
import ProductForm from '../../components/ProductForm'
// import Back from '../../component/Back'
import { createProduct } from '../../api/api'

export default function CreateProduct(props) {

    const save = async (user) => {
        let result = await createProduct(user)
        props.history.push('/home')
      }

      return (
        <div>
          {/* <Back url="/product" history={props.history}/> */}
          <h1>Create Product</h1>
          <hr/>
          <ProductForm  save={save}/>
        </div>
      )
}
