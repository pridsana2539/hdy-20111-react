import React from 'react'

export default function HomeTitle() {
    return (
        <div>
            <h1>All users</h1>
            <hr/>
        </div>
    )
}
