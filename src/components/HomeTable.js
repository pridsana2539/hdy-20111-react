import React from "react";
import { Link } from "react-router-dom";

export default function HomeTable(props) {
  const tableList = () => {
    if (props.user.data !== undefined) {
      var data = [];
      for (let i = 0; i < props.user.data.length; i++) {
        let item = props.user.data[i];
        data.push(
          <tr>
            <th scope="row">{i + 1}</th>
            <td>{item.name}</td>
            <td>{item.age}</td>
            <td>{item.salary}</td>
            <td>
              <Link to={`/edit/${item._id}`}>
                <span style={{ color: "green" }}>Edit</span>
              </Link>
              |
              <span onClick={() => props.delete(item._id)} style={{ color: "red", cursor: 'pointer'}} >Delete</span>
            </td>
          </tr>
        );
      }
      return data;
    }
  };

  return (
    <div>
      <table class="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Salary</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>{tableList()}</tbody>
      </table>
    </div>
  );
}
