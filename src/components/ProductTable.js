import React from "react";
// import { Link } from "react-router-dom";

export default function ProductTable(props) {
    const tableList = () => {
        if (props.product.data !== undefined) {
          var data = [];
          for (let i = 0; i < props.product.data.length; i++) {
            let item = props.product.data[i];
            data.push(
              <tr >
                <th  scope="row">{i + 1}</th>
                <td>{item.title}</td>
                <td>{item.detail}</td>
                <td>{item.stock}</td>
                <td>{item.price}</td>
                {/* <td>
                  <Link to={`/edit/${item._id}`}>
                    <span style={{ color: "green" }}>Edit</span>
                  </Link>
                  |
                  <span onClick={() => props.delete(item._id)} style={{ color: "red", cursor: 'pointer'}} >Delete</span>
                </td> */}
              </tr>
            );
          }
          return data;
        }
      };
    
      return (
        <div>
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">No.</th>
                <th scope="col">title</th>
                <th scope="col">detail</th>
                <th scope="col">stock</th>
                <th scope="col">price</th>
                {/* <th scope="col">Actions</th> */}
              </tr>
            </thead>
            <tbody>{tableList()}</tbody>
          </table>
        </div>
      );
    }
    