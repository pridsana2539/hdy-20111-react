import React from 'react'

export default function HomeCreateButton(props) {
  return (
    <div>
      <button className="btn btn-success " onClick={() => { props.nextCreate() } }>Create Products</button>
      {/* <button className="btn btn-info " onClick={() => { props.nextMyProduct() } }>My Products</button> */}
    </div>
  )
}
