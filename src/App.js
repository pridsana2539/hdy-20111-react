import React from 'react';
import Header from './components/Header'
import Login from './pages/Login/Login'
import Register from './pages/Register/Register'
import Home from './pages/Home/Home'
import Create from './pages/Create/Create'
import Edit from './pages/Edit/Edit'
import Showprofile from './pages/Home/Showprofile'
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom'
import PrivateRoute from './helper/PrivateRoute'
import Profile from './pages/Profile/Profile'
import CreateProduct from './pages/Create/CreateProduct'
import Product from './pages/Home/Product'
import EditProduct from './pages/Edit/EditProduct'
import Editprofile from './pages/Edit/Editprofile'


var routes = {
  login: '/login',
  register: '/register',
  home: '/home',
  create: '/create',
  edit: '/edit/:id',
  showprofile: '/showprofile',
  profile: '/profile',
  product: '/product',
  createProduct:'/createProduct',
  editProfile:'/editprofile/:id',
  editProduct: '/editproduct/:id'

}

function App() {
  return (
    <div>
      <Header/>
      <div className="container login">
          <Switch>
            <Redirect exact from="/" to={routes.login}></Redirect>
            <Route exact path={routes.login} component={Login}></Route>
            <Route exact path={routes.register} component={Register}></Route>
          </Switch>
      </div>
      <div className="container">
          <Switch>
              <PrivateRoute exact path={routes.home} component={Home}></PrivateRoute>
              <PrivateRoute exact path={routes.create} component={Create}></PrivateRoute>
              <PrivateRoute exact path={routes.edit} component={Edit}></PrivateRoute>
              <PrivateRoute exact path={routes.showprofile} component={Showprofile}></PrivateRoute>
              <PrivateRoute exact path={routes.profile} component={Profile}></PrivateRoute>
              <PrivateRoute exact path={routes.product} component={Product}></PrivateRoute>
              <PrivateRoute exact path={routes.createProduct} component={CreateProduct}></PrivateRoute>
              <PrivateRoute exact path={routes.editProfile} component={Editprofile}></PrivateRoute>
              <PrivateRoute exact path={routes.editProduct} component={EditProduct}></PrivateRoute>
          </Switch>
      </div>
    </div>
  );
}

export default App; 
